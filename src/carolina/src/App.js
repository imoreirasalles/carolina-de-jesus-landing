import './App.scss';
import GridPink from './GridPink'
import GridGreen from './GridGreen'
import GridBlue from './GridBlue'

function App() {
  const randomize = (myArray) => {
    return myArray[Math.floor(Math.random() * myArray.length)];
  }
  const arr = [
    <GridPink />,
    <GridGreen />,
    <GridBlue />
  ]
  return (
    <>
      { randomize(arr) }
      <footer className='footer'>
        <img src='./images/logo-ims.png' alt='Instituto Moreira Salles' />
      </footer>
    </>
  );
}
export default App;