import React, { useEffect, useRef } from "react";
import './overlay.scss';

export default function Overlay() {
  const scrollingContainerRef = useRef();

  const data = {
    ease: 0.1,
    current: 0,
    previous: 0,
    rounded: 0,
  };

  useEffect(() => {
    requestAnimationFrame(() => smoothScrollingHandler());
  }, []);

  const smoothScrollingHandler = () => {
    data.current = window.scrollY;
    data.previous += (data.current - data.previous) * data.ease;
    data.rounded = Math.round(data.previous * 100) / 100;

    scrollingContainerRef.current.style.transform = `translateY(-${data.previous}px)`;
    requestAnimationFrame(() => smoothScrollingHandler());
  };

  return (
    <div className='overlay'>
      <div ref={scrollingContainerRef}>
        <span>Carolina</span>
        <span>Maria</span>
        <span>de Jesus</span>
        <span>Jesus</span>
        <span>Carolina</span>
        <span>Maria</span>
        <span>de Jesus</span>
        <span>Jesus</span>
        <span>Carolina</span>
        <span>Maria</span>
        <span>de Jesus</span>
        <span>Jesus</span>
      </div>
    </div>
  );
}