import './grid.scss';
import Overlay from '../Overlay'

export default function Grid() {
  return (
    <main className='grid green'>

      <section className='column'>
        <div className='title container'>
          <h1>Carolina <br/> Maria <br/> de Jesus</h1>
        </div>

        <figure 
          className='container container_01 image'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_01.jpg' alt='Carolina Maria de Jesus à beira do Tietê' />
          <figcaption>
            <div className='legend'>
              <h2>“Carolina Maria de Jesus à beira do Tietê”.</h2>
              <p>Autoria não identificada. 1960. Arquivo Nacional, Fundo Correio da Manhã, Rio de Janeiro.</p>
            </div>
          </figcaption>
        </figure>
      </section>

      <section className='column'>
        <figure 
          className='container container_02 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_03.jpg' alt='Ruth &amp; Carolina' />
          <figcaption>
            <div className='legend'>
              <h2>“Ruth &amp; Carolina”.</h2>
              <p>No Martins. 2020. Pintura sobre tela.</p>
            </div>
          </figcaption>
        </figure>

        <div 
          className='container container_03'
          data-aos='fade-up-left'
        >
          <p>"A vida <span>nao e para os covardes.</span>"</p>
        </div>
      </section>

      <section className='column column_span'>
        <figure 
          className='container container_04 image'
          data-aos-offset='500'
          data-aos-easing='linear'
        >
          <img src='./images/carolina_13.jpg' alt='A outra Carolina tem nome feito de Lantejoulas' />
          <figcaption>
            <div className='legend'> 
              <h2>“A outra Carolina tem nome feito de Lantejoulas”.</h2>
              <p>Texto e foto de Zélia Gattai. 1961. Acervo Casa de Jorge Amado e Zélia Gattai.</p>
            </div>
          </figcaption>
        </figure>
      </section>

      <section className='column'>
        <figure 
          className='container container_05 image'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_07.jpg' alt='A ancestral do futuro' />
          <figcaption>
            <div className='legend'>
              <h2>“A ancestral do futuro”.</h2>
              <p>Criola. 2021. Tinta acrílica sobre parede.</p>
            </div>
          </figcaption>
        </figure>

        <figure 
          className='container container_06 image'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_10.jpg' alt='Retrato de Carolina Maria de Jesus junto a sua filha Vera Eunice, o presidente João Goulart e sujeito não identificado' />
          <figcaption>
            <div className='legend'>
              <h2>“Retrato de Carolina Maria de Jesus junto a sua filha Vera Eunice, o presidente João Goulart e sujeito não identificado”.</h2>
              <p>Autoria não identificada. 1961. Arquivo Nacional, Fundo Correio da Manhã, Rio de Janeiro.</p>
            </div>
          </figcaption>
        </figure>

        <div 
          className='container container_07'
          data-aos='fade-up-right'
        >
          <p>"Não estou com sono porque eu tenho sono durante o dia.<span> E a noite tenho poesia.</span>"</p>
        </div>

        <figure 
          className='container container_08 image'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_04.jpg' alt='Carolina na RCA' />
          <figcaption>
            <div className='legend'>
              <h2>“Carolina na RCA”.</h2>
              <p>TV Radiolândia, Rio de Janeiro, 1961. Acervo Fundação Biblioteca Nacional.</p>
            </div>
          </figcaption>
        </figure>
      </section>

      <section className='column top-120'>
        <figure 
          className='container container_09 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_02.jpg' alt='Meada' />
          <figcaption>
            <div className='legend'> 
              <h2>“Meada”.</h2>
              <p>Antônio Obá. 2021. Óleo sobre tela.</p>
            </div>
          </figcaption>
        </figure>

        <div 
          className='container container_10'
          data-aos='fade-up-left'
        >
          <p className='font-50'>"Estou perdendo aquela capacidade belíssima para escrever. Escrever é arte <span>Mas, quando a arte passa a ser comercializada perde o encanto.</span>"</p>
        </div>

        <figure 
          className='container container_11 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_09.jpg' alt='Poema “Negros”. Carolina Maria de Jesus.' />
          <figcaption>
            <div className='legend'>
              <h2>Poema “Negros”. Carolina Maria de Jesus.</h2>
              <p>Manuscrito. Acervo da autora em Sacramento-MG. </p>
            </div>
          </figcaption>
        </figure>

        <figure 
          className='container container_12 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_06.jpg' alt='Carolina Maria de Jesus com Dirce Machado no Clube Renascença no Rio de Janeiro' />
          <figcaption>
            <div className='legend'>
              <h2>“Carolina Maria de Jesus com Dirce Machado no Clube Renascença no Rio de Janeiro”.</h2>
              <p>Autoria não identificada. 1960. Coleção Arquivo Nacional, Rio de Janeiro.</p>
            </div>
          </figcaption>
        </figure>
      </section>

      <Overlay />
    </main>
  );
}