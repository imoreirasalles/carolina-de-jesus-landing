import './grid.scss';
import Overlay from '../Overlay'

export default function Grid() {
  return (
    <main className='grid blue'>

      <section className='column'>
        <div className='title container'>
          <h1>Carolina <br/> Maria <br/> de Jesus</h1>
        </div>

        <figure 
          className='container container_01 image top-120'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_03.jpg' alt='Ruth &amp; Carolina' />
          <figcaption>
            <div className='legend'>
              <h2>“Ruth &amp; Carolina”.</h2>
              <p>No Martins. 2020. Pintura sobre tela.</p>
            </div>
          </figcaption>
        </figure>

        <div 
          className='container container_02'
          data-aos='fade-up-right'
        >
          <p>"Ambição de preto é a <span>tranquilidade. a paz.</span>"</p>
        </div>

        <figure 
          className='container container_03 image'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_01.jpg' alt='Carolina Maria de Jesus à beira do Tietê' />
          <figcaption>
            <div className='legend'>
              <h2>“Carolina Maria de Jesus à beira do Tietê”.</h2>
              <p>Autoria não identificada. 1960. Arquivo Nacional, Fundo Correio da Manhã, Rio de Janeiro.</p>
            </div>
          </figcaption>
        </figure>

        <figure 
          className='container container_04 image'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_02.jpg' alt='Meada' />
          <figcaption>
            <div className='legend'>
              <h2>“Meada”.</h2>
              <p>Antônio Obá. 2021. Óleo sobre tela.</p>
            </div>
          </figcaption>
        </figure>
      </section>

      <section className='column'>
        <figure 
          className='container container_05 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_06.jpg' alt='Carolina Maria de Jesus com Dirce Machado no Clube Renascença no Rio de Janeiro' />
          <figcaption>
            <div className='legend'>
              <h2>“Carolina Maria de Jesus com Dirce Machado no Clube Renascença no Rio de Janeiro”.</h2>
              <p>Autoria não identificada. 1960. Coleção Arquivo Nacional, Rio de Janeiro.</p>
            </div>
          </figcaption>
        </figure>

        <figure 
          className='container container_06 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_09.jpg' alt='Poema “Negros”. Carolina Maria de Jesus' />
          <figcaption>
            <div className='legend'>
              <h2>Poema “Negros”. Carolina Maria de Jesus.</h2>
              <p>Manuscrito. Acervo da autora em Sacramento-MG. </p>
            </div>
          </figcaption>
        </figure>

        <div 
          className='container container_07'
          data-aos='fade-up-left'
        >
          <p className='font-50'>"Eu tenho a impressão que a vida do brasileiro é como uma roupa que rompe-se,<span> que nao tem mais conserto, e a gente precisa dela.</span>"</p>
        </div>
      </section>

      <section className='column column_span'>
        <figure 
          className='container container_08 image'
          data-aos-offset='500'
          data-aos-easing='linear'
        >
          <img src='./images/carolina_11.jpg' alt='Ruth virou Carolina' />
          <figcaption>
            <div className='legend'>
              <h2>“Ruth virou Carolina”.</h2>
              <p>Reportagem de Audálio Dantas, Fotos de George Torok. O Cruzeiro. 1961. Acervo Diários dos Associados.</p>
            </div>
          </figcaption>
        </figure>
      </section>

      <section className='column'>
        <div 
          className='container container_09'
          data-aos='fade-up-right'
        >
          <p className='font-60'>"A vida <span> Nao e para os covardes.</span>"</p>
        </div>

        <figure 
          className='container container_10 image'
          data-aos='fade-up-right'
        >
          <img src='./images/carolina_07.jpg' alt='A ancestral do futuro' />
          <figcaption>
            <div className='legend'>
              <h2>“A ancestral do futuro”.</h2>
              <p>Criola. 2021. Tinta acrílica sobre parede.</p>
            </div>
          </figcaption>
        </figure>

        <div 
          className='container container_11'
          data-aos='fade-up-right'
        >
          <p className='font-60'>"Eu queria viver seculos e seculos para ler todos os livros que ha no glôbo. <span>Eu  quero o meu tumulo no formato de um livro.</span> Igual o quarto de despêjo."</p>
        </div>
      </section>

      <section className='column'>
        <figure 
          className='container container_12 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_10.jpg' alt='Retrato de Carolina Maria de Jesus junto a sua filha Vera Eunice, o presidente João Goulart e sujeito não identificado' />
          <figcaption>
            <div className='legend'>
              <h2>“Retrato de Carolina Maria de Jesus junto a sua filha Vera Eunice, o presidente João Goulart e sujeito não identificado”.</h2>
              <p>Autoria não identificada. 1961. Arquivo Nacional, Fundo Correio da Manhã, Rio de Janeiro.</p>
            </div>
          </figcaption>
        </figure>

        <figure 
          className='container container_13 image'
          data-aos='fade-up-left'
        >
          <img src='./images/carolina_05.jpg' alt='Te imagino, Carolina' />
          <figcaption>
            <div className='legend'>
              <h2>“Te imagino, Carolina”.</h2>
              <p>Silvana Mendes. 2021. Colagem digital.</p>
            </div>
          </figcaption>
        </figure>
      </section>

      <Overlay />
    </main>
  );
}